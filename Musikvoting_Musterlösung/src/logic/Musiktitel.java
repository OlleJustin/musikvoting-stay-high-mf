package logic;

/**
  * Datenklasse der Musik
  *
  * @version 1.0 vom 31.05.2016
  * @author Tenbusch
  */

public class Musiktitel {
  
  // Anfang Attribute
  private String bandname;
  private String titelname;
  private String genre;
  // Ende Attribute
  
  public Musiktitel(String bandname, String titelname, String genre) {
    this.bandname = bandname;
    this.titelname = titelname;
    this.genre = genre;
  }

  // Anfang Methoden
  public String getBandname() {
    return bandname;
  }

  public void setBandname(String bandname) {
    this.bandname = bandname;
  }

  public String getTitelname() {
    return titelname;
  }

  public void setTitelname(String titelname) {
    this.titelname = titelname;
  }

  public String getGenre() {
    return genre;
  }

  public void setGenre(String genre) {
    this.genre = genre;
  }

@Override
public String toString() {
	return "Musik [bandname=" + bandname + ", titelname=" + titelname + ", genre=" + genre + "]";
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((bandname == null) ? 0 : bandname.hashCode());
	result = prime * result + ((genre == null) ? 0 : genre.hashCode());
	result = prime * result + ((titelname == null) ? 0 : titelname.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Musiktitel other = (Musiktitel) obj;
	if (bandname == null) {
		if (other.bandname != null)
			return false;
	} else if (!bandname.equals(other.bandname))
		return false;
	if (genre == null) {
		if (other.genre != null)
			return false;
	} else if (!genre.equals(other.genre))
		return false;
	if (titelname == null) {
		if (other.titelname != null)
			return false;
	} else if (!titelname.equals(other.titelname))
		return false;
	return true;
}

  // Ende Methoden
} // end of Music
