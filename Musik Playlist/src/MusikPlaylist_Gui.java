import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

public class MusikPlaylist_Gui extends JFrame {

	private JPanel contentPane;
	private JTextField txtMusik;
	private JTextField txtCxv;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JPanel panel_5;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JButton btnHinzufgen;
	private JPanel panel_6;
	private JList list;
	private JLabel lblNewLabel_3;
	private JButton btnVote;
	private JPanel panel_7;
	private JButton btnNewButton;
	private JPanel panel_9;
	private JPanel panel_10;
	private JLabel lblNewLabel_5;
	private JRadioButton rdbtnNewRadioButton;
	private JRadioButton rdbtnNewRadioButton_1;
	private JPanel panel_11;
	private JLabel lblNewLabel_6;
	private JTextField textField;
	private JButton btnE;
	private JPanel panel_12;
	private DefaultListModel listModel;
	private JPanel panel_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusikPlaylist_Gui frame = new MusikPlaylist_Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MusikPlaylist_Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 616, 422);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	

			
		
		//Anzeige Panel
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0,590,383);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
			txtMusik = new JTextField();
		panel.add(txtMusik);
		txtMusik.setText("Musik");
		txtMusik.setColumns(10);
		ImageIcon myfile = new ImageIcon("C:\\Users\\user\\Downloads\\Mainbild.jpg");
		JLabel Bild = new JLabel(myfile);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panel.add(tabbedPane);
		
		panel_9 = new JPanel();
		tabbedPane.addTab("Einloggen", null, panel_9, null);
		
		panel_10 = new JPanel();
		panel_9.add(panel_10);
		
		lblNewLabel_5 = new JLabel("Gastgeber");
		panel_10.add(lblNewLabel_5);
		
		rdbtnNewRadioButton = new JRadioButton("ja");
		panel_10.add(rdbtnNewRadioButton);
		
		rdbtnNewRadioButton_1 = new JRadioButton("nein");
		panel_10.add(rdbtnNewRadioButton_1);
		
		panel_11 = new JPanel();
		panel_9.add(panel_11);
		
		lblNewLabel_6 = new JLabel("Gastname:");
		panel_11.add(lblNewLabel_6);
		
		textField = new JTextField();
		panel_11.add(textField);
		textField.setColumns(10);
		
		btnE = new JButton("Einloggen");
		panel_9.add(btnE);
		
	
		
		
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Song hinzufügen", null, panel_1, null);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(new GridLayout(2, 1, 0, 0));
		
		panel_4 = new JPanel();
		panel_3.add(panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[]{97, 97, 97, 97, 97, 97, 0};
		gbl_panel_4.rowHeights = new int[]{81, 0};
		gbl_panel_4.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_4.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_4.setLayout(gbl_panel_4);
		
		lblNewLabel_2 = new JLabel("Titel");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 0;
		panel_4.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 0, 5);
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 0;
		panel_4.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		lblNewLabel = new JLabel("Interpret");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.gridx = 2;
		gbc_lblNewLabel.gridy = 0;
		panel_4.add(lblNewLabel, gbc_lblNewLabel);
		
		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.insets = new Insets(0, 0, 0, 5);
		gbc_textField_2.gridx = 3;
		gbc_textField_2.gridy = 0;
		panel_4.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Genre");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_1.gridx = 4;
		gbc_lblNewLabel_1.gridy = 0;
		panel_4.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		txtCxv = new JTextField();
		GridBagConstraints gbc_txtCxv = new GridBagConstraints();
		gbc_txtCxv.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCxv.gridx = 5;
		gbc_txtCxv.gridy = 0;
		panel_4.add(txtCxv, gbc_txtCxv);
		txtCxv.setColumns(10);
		
		panel_5 = new JPanel();
		panel_3.add(panel_5);
		
		btnHinzufgen = new JButton("Hinzuf\u00FCgen");
		panel_5.add(btnHinzufgen);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Voten", null, panel_2, null);
		
		panel_6 = new JPanel();
		panel_6.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_2.add(panel_6);
		
		
		panel_6.setLayout(new GridLayout(0, 2, 0, 0));


		
		panel_6.add(new JLabel("Eminem Lose Yourself"));
		
		panel_6.add(new JButton("Vote"));
		
		panel_6.add(new JLabel("Migos Superfly"));
		
		panel_6.add(new JButton("Vote"));
		
		panel_6.add(new JLabel("Capital Wieder Lila "));
		
		panel_6.add(new JButton("Vote"));
		
		panel_6.add(new JLabel("Travis Scott Butterfly"));
		
		panel_6.add(new JButton("Vote"));

		
		
		
		
		panel_7 = new JPanel();
		tabbedPane.addTab("Ausgabe", null, panel_7, null);
panel_7.setLayout(new GridLayout(0, 1, 0, 0));

btnNewButton = new JButton("Ausgabe");
panel_7.add(btnNewButton);
		
		panel_8 = new JPanel();
		panel_7.add(panel_8);
panel_8.setLayout(new GridLayout(0, 1, 0, 0));
		
JLabel label = new JLabel("Eminem Lose Yourself");
panel_8.add(label);



JLabel label_1 = new JLabel("Migos Superfly");
panel_8.add(label_1);
		
		
			
	}

}
